package com.rrg.randomdinnergnerator.api



import com.rrg.randomdinnergnerator.BuildConfig
import com.rrg.randomdinnergnerator.data.food.FoodCategoriesResponse
import com.rrg.randomdinnergnerator.data.food.MealDetailsResponse
import com.rrg.randomdinnergnerator.data.food.MealResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface MealAPI {

    companion object{
        const val BASE_URL = "https://www.themealdb.com"
        const val API_KEY = BuildConfig.FOOD_API_KEY
    }

    @GET("/api/json/v1/$API_KEY/categories.php")
    fun getFoodCategories(): Observable<FoodCategoriesResponse>

    @GET("/api/json/v1/$API_KEY/filter.php")
    fun getFoodListByCategory(
        @Query("c")category: String
    ): Observable<MealResponse>

    @GET("/api/json/v1/$API_KEY/lookup.php")
    fun getMealDetails(
        @Query("i") mealID:String
    ): Observable<MealDetailsResponse>

}