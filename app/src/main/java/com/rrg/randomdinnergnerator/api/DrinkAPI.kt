package com.rrg.randomdinnergnerator.api

import com.rrg.randomdinnergnerator.BuildConfig
import com.rrg.randomdinnergnerator.data.drinks.DrinkCategoriesResponse
import com.rrg.randomdinnergnerator.data.drinks.DrinkDetailsResponse
import com.rrg.randomdinnergnerator.data.drinks.DrinkResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface DrinkAPI {
    companion object{
        const val BASE_URL="https://www.thecocktaildb.com/"
        const val API_KEY = BuildConfig.FOOD_API_KEY
    }

    @GET("/api/json/v1/${API_KEY}/list.php?c=list")
    fun getDrinkCategories():Observable<DrinkCategoriesResponse>

    @GET("/api/json/v1/${API_KEY}/filter.php")
    fun getDrinkByCategory(
        @Query("c") category: String
    ): Observable<DrinkResponse>

    @GET("/api/json/v1/${API_KEY}/lookup.php")
    fun getDrinkDetails(
        @Query("i") drinkID:String
    ): Observable<DrinkDetailsResponse>
}