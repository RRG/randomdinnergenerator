package com.rrg.randomdinnergnerator

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RandomDinnerApplicationClass:Application() {
}