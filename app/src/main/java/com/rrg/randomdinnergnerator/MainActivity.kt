package com.rrg.randomdinnergnerator

import android.content.Context
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var navigationController:NavController
    private lateinit var navigationHostFragment:NavHostFragment

    //override phone text size settings (accessibility)
    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(newBase)
        val newOverride = Configuration(newBase?.resources?.configuration)
        newOverride.fontScale = 1.0f
        applyOverrideConfiguration(newOverride)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_RandomDinnerGnerator)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigationHostFragment = supportFragmentManager.findFragmentById(R.id.navigation_Host) as NavHostFragment
        navigationController = navigationHostFragment.findNavController()
    }

    override fun onSupportNavigateUp(): Boolean {
        return navigationController.navigateUp() || super.onSupportNavigateUp() //handle back press
    }
}