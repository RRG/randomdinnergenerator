package com.rrg.randomdinnergnerator.ui.home

import android.os.*
import android.transition.TransitionInflater
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.rrg.randomdinnergnerator.R
import com.rrg.randomdinnergnerator.data.enums.DinnerTypes
import com.rrg.randomdinnergnerator.databinding.HomePageBinding
import com.rrg.randomdinnergnerator.ui.home.childFragments.DrinkCategoryFragment
import com.rrg.randomdinnergnerator.ui.home.childFragments.FoodCategoryFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class Home : Fragment(R.layout.home_page){

    private val viewModel by viewModels<HomeViewModel>()
    private lateinit var binding: HomePageBinding

    override fun onCreate(savedInstanceState: Bundle?) {   //shared elements transition movement
        super.onCreate(savedInstanceState)
        val anim = TransitionInflater.from(requireContext()).inflateTransition(
            android.R.transition.move
        )
        sharedElementEnterTransition = anim
        sharedElementReturnTransition = anim

        //called here coz it will be done only once for the app's lifecycle instead of everytime the fragments are loaded
        viewModel.getFoodCategories()
        viewModel.getDrinkCategories()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = HomePageBinding.bind(view)

        requireActivity().onBackPressedDispatcher.addCallback {
            requireActivity().finishAndRemoveTask() //to avoid going back to the splash
        }


        binding.apply {

            toggleParent.addOnButtonCheckedListener { _, checkedId, isChecked ->
                if(isChecked){
                    when(checkedId){
                        R.id.btn_food ->{ viewModel.currentView.value = DinnerTypes.FOOD }
                        R.id.btn_drinks->{ viewModel.currentView.value = DinnerTypes.DRINK }
                    }
                }
            }

            viewModel.currentView.observe(viewLifecycleOwner){ //listen to toggle changes via ENUMS
                when(it){
                    DinnerTypes.FOOD->{
                        initFoodFragment()
                    }
                    DinnerTypes.DRINK->{
                       initDrinkFragment()
                    }
                    else->{
                        //nothin
                    }
                }
            }

            viewModel.failedFetch.observe(viewLifecycleOwner){
                if(it){ //API request failed
                    Toast.makeText(requireContext(),"error", Toast.LENGTH_SHORT).show()
                }
            }

            btnSuggestDinner.setOnClickListener {
                if (viewModel.userSelection.selectedDrinkCategory != null && viewModel.userSelection.selectedFoodCategory != null){
                    val action = HomeDirections.actionHome2ToRecommendationPage(viewModel.userSelection)
                    val extras = FragmentNavigatorExtras(tvAppTitle to "name",
                                                            divider to "div")
                    findNavController().navigate(action,extras)
                }else{

                    // error message -> shake toggle + let user know via a msg that they need to select categories
                    tvError.visibility = View.VISIBLE
                    val shakeAnim = AnimationUtils.loadAnimation(requireContext(),R.anim.shake)
                    toggleParent.startAnimation(shakeAnim)
                    Handler(Looper.getMainLooper()).postDelayed({
                        tvError.visibility = View.INVISIBLE
                       },3000) //show error message for 3 seconds only
                }
            }

        }
    }

    /**
     * method to handle proper initialisation of the fragment which contains the recycler view and adapter for FOOD
     */
    private fun initFoodFragment(){
        binding.apply {
            childFragmentManager.beginTransaction()
                .replace(fragmentContainerHome.id, FoodCategoryFragment())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit()
        }
    }

    /**
     * method to handle proper initialisation of the fragment which contains the recycler view and adapter for DRINKS
     */
    private fun initDrinkFragment(){
        binding.apply {
            childFragmentManager.beginTransaction()
                .replace(fragmentContainerHome.id, DrinkCategoryFragment())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit()
        }
    }


}