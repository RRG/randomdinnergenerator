package com.rrg.randomdinnergnerator.ui.recommendation

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.rrg.randomdinnergnerator.R
import com.rrg.randomdinnergnerator.data.RecommendedDinner
import com.rrg.randomdinnergnerator.data.drinks.Drink
import com.rrg.randomdinnergnerator.data.food.Meal
import com.rrg.randomdinnergnerator.databinding.RecommendationPageBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class RecommendationPage:Fragment(R.layout.recommendation_page) {

    private val viewModel by viewModels<RecommendationViewModel>()
    private val args by navArgs<RecommendationPageArgs>()
    private lateinit var binding:RecommendationPageBinding
    private lateinit var randomFood : Meal
    private lateinit var randomDrink : Drink

    override fun onCreate(savedInstanceState: Bundle?) {   //shared elements transition movement
        super.onCreate(savedInstanceState)
        val anim = TransitionInflater.from(requireContext()).inflateTransition(
            android.R.transition.move
        )
        sharedElementEnterTransition = anim
        sharedElementReturnTransition = anim
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback {
            findNavController().navigateUp() //on pressing back button on phone
        }

        binding  = RecommendationPageBinding.bind(view)

        //fetch all food & drink list data based on category selected
        args.userSelection.selectedFoodCategory?.let { viewModel.getFoodByCategory(it) }
        args.userSelection.selectedDrinkCategory?.let { viewModel.getDrinkByCategory(it) }

        binding.apply {

            btnRecommendationBack.setOnClickListener {  ///handle back button
                findNavController().navigateUp()
            }

            //clicking on recommended dinner
            ivFoodRecommendation.setOnClickListener {
                val action = RecommendationPageDirections
                    .actionRecommendationPageToRecommendationDetailDialog(RecommendedDinner(randomFood.idMeal,null))
                findNavController().navigate(action)
            }
            ivDrinkRecommendation.setOnClickListener {
                val action = RecommendationPageDirections
                    .actionRecommendationPageToRecommendationDetailDialog(RecommendedDinner(null,randomDrink.idDrink))
                findNavController().navigate(action)
            }

            viewModel.combinedSuccessLists.observe(viewLifecycleOwner){
                if(it){//all data fetched
                    progressRecommendation.visibility = View.GONE //stop loading progress bar
                    clRecommendationCardParent.visibility = View.VISIBLE //showView

                    //get first random selections
                    randomFood = viewModel.getRandomFood()!!
                    randomDrink = viewModel.getRandomDrink()!!

                    //activate refresh button
                    btnSuggestAnotherRecommendation.isEnabled = true

                    //set views
                    updateRecommendationViews()

                    viewModel.combinedSuccessLists.removeObservers(viewLifecycleOwner)
                }
            }

            viewModel.failedFetch.observe(viewLifecycleOwner){
                if(it){ //API request failed
                    progressRecommendation.visibility = View.GONE //stop loading progress bar
                    Toast.makeText(requireContext(),"error", Toast.LENGTH_SHORT).show()
                }
            }

            btnSuggestAnotherRecommendation.setOnClickListener {
                //choose another random dinner(food & drink)
                randomFood = viewModel.getRandomFood()!!
                randomDrink = viewModel.getRandomDrink()!!
                updateRecommendationViews() //update views with new recommendation
            }

        }

    }

    /**
     * Method that handles updating the views of the dinner recommendation
     */
    private fun updateRecommendationViews() {
      binding.apply {
          clRecommendationCardParent.animation = AnimationUtils.loadAnimation(requireContext(),R.anim.flip)

          val requestOptions = RequestOptions.bitmapTransform( RoundedCorners(14))

          Glide.with(requireContext())
              .load(randomFood.strMealThumb)
              .apply(requestOptions)
              .thumbnail(Glide.with(requireContext()).load(R.raw.downloading).apply(requestOptions)) //placeholder
              .transition(DrawableTransitionOptions.withCrossFade())
              .into(ivFoodRecommendation)

          Glide.with(requireContext())
              .load(randomDrink.strDrinkThumb)
              .apply(requestOptions)
              .thumbnail(Glide.with(requireContext()).load(R.raw.downloading).apply(requestOptions)) //placeholder
              .transition(DrawableTransitionOptions.withCrossFade())
              .into(ivDrinkRecommendation)

          tvRecommendationFoodName.setText(randomFood.strMeal)
          tvRecommendationDrinkName.setText(randomDrink.strDrink)
      }
    }

}