package com.rrg.randomdinnergnerator.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rrg.randomdinnergnerator.R
import com.rrg.randomdinnergnerator.data.drinks.DrinkCategories
import com.rrg.randomdinnergnerator.databinding.CategoryRecordItemSmallBinding

class DrinkAdapter(
    private val listener: DrinkCardListeners
): ListAdapter<DrinkCategories, DrinkAdapter.DrinkViewHolder>(DiffCallback()){

    interface DrinkCardListeners{
        fun onDrinkCardClicked(data: DrinkCategories)
        fun getSelectedDrink():String
    }


    inner class DrinkViewHolder(
        private val binding: CategoryRecordItemSmallBinding
    ): RecyclerView.ViewHolder(binding.root){

        init {
            binding.apply {
                //handle on card click
                root.setOnClickListener {
                    val record = getItem(adapterPosition)
                    root.isSelected = true
                    tvCardItemName.setTextColor(ContextCompat.getColor(root.context, R.color.white))
                    listener.onDrinkCardClicked(record)
                }
            }
        }

        fun bindViews(record: DrinkCategories){
            binding.apply {
                //assign data to each card
                tvCardItemName.setText(record.strCategory)
                //Will retain previous selection in UI, and unselect previous items
                if(record.strCategory == listener.getSelectedDrink()){
                    root.isSelected = true
                    tvCardItemName.setTextColor(ContextCompat.getColor(root.context, R.color.white))
                }else{
                    root.isSelected = false
                    tvCardItemName.setTextColor(ContextCompat.getColor(root.context, R.color.blue))
                }
            }
        }

    }


    class DiffCallback:DiffUtil.ItemCallback<DrinkCategories>(){
        override fun areItemsTheSame(oldItem: DrinkCategories, newItem: DrinkCategories): Boolean {
           return oldItem.strCategory == oldItem.strCategory
        }

        override fun areContentsTheSame(oldItem: DrinkCategories, newItem: DrinkCategories): Boolean {
            return oldItem==newItem
        }
    }

    override fun onCreateViewHolder(parent:ViewGroup, viewType:Int):DrinkViewHolder{
        val binding = CategoryRecordItemSmallBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return DrinkViewHolder(binding)
    }

    override fun onBindViewHolder(holder:DrinkViewHolder, position:Int){
        val currwntItem = getItem(position)
        holder.bindViews(currwntItem)
    }
}