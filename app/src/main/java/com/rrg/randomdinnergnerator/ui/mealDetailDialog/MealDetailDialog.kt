package com.rrg.randomdinnergnerator.ui.mealDetailDialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.rrg.randomdinnergnerator.R
import com.rrg.randomdinnergnerator.databinding.FoodDetailsDialogBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MealDetailDialog:DialogFragment() {

    private val args by navArgs<MealDetailDialogArgs>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val builder = AlertDialog.Builder(requireActivity(),R.style.myFoodDetailDialog)
        val binding: FoodDetailsDialogBinding =
            FoodDetailsDialogBinding.inflate(LayoutInflater.from(context))
        builder.setView(binding.root)

        binding.apply {

            tvFoodDetailName.setText(args.foodDetails.strCategory)
            tvFoodDetailDescription.setText(args.foodDetails.strCategoryDescription)
            tvFoodDetailDescription.movementMethod = ScrollingMovementMethod()

            Glide.with(requireContext())
                .load(args.foodDetails.strCategoryThumb)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(ivFoodDetailThumb)

            btnCloseFoodDetails.setOnClickListener { //close button
                findNavController().navigateUp()
            }

        }
        return builder.create()
    }


}