package com.rrg.randomdinnergnerator.ui.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.rrg.randomdinnergnerator.R
import com.rrg.randomdinnergnerator.databinding.SplashScreenBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashScreen:Fragment(R.layout.splash_screen) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = SplashScreenBinding.bind(view)

        //load splash animations
        val topAnim = AnimationUtils.loadAnimation(requireContext(),R.anim.top_anim)
        val bottomAnim = AnimationUtils.loadAnimation(requireContext(),R.anim.bottom_anim)

        binding.apply {

            ivSplashLogo.animation = topAnim
            tvSplashSlogan.animation = bottomAnim

            Handler(Looper.getMainLooper()).postDelayed({
                val action = SplashScreenDirections.actionSplashScreenToHome2()
                val extras = FragmentNavigatorExtras(ivSplashLogo to "logo",
                                                                tvSplashSlogan to "name")
                findNavController().navigate(action, extras)
            },1500) //make user watch animation for 1.5 seconds
        }

    }
}