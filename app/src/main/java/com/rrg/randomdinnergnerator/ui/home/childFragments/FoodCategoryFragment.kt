package com.rrg.randomdinnergnerator.ui.home.childFragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rrg.randomdinnergnerator.R
import com.rrg.randomdinnergnerator.data.food.FoodCategories
import com.rrg.randomdinnergnerator.databinding.FoodCategoryFragmentBinding
import com.rrg.randomdinnergnerator.ui.home.HomeDirections
import com.rrg.randomdinnergnerator.ui.home.HomeViewModel
import com.rrg.randomdinnergnerator.ui.home.MealAdapter

class FoodCategoryFragment:Fragment(R.layout.food_category_fragment), MealAdapter.MealCardListeners {

    private lateinit var mealAdapter:MealAdapter
    private val viewModel by viewModels<HomeViewModel>(
        {requireParentFragment()}  //sharing viewModel with parent
    )


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FoodCategoryFragmentBinding.bind(view)

        mealAdapter = MealAdapter(this)

        binding.apply {
            rvFoodItemList.layoutManager = LinearLayoutManager(requireActivity())
            rvFoodItemList.adapter = mealAdapter
            rvFoodItemList.setHasFixedSize(true)

            viewModel.fetchSuccessFood.observe(viewLifecycleOwner){
                if (it){ //data fetched

                    //hide loading progress bar
                    loading.visibility = View.GONE

                    viewModel.fetchSuccessFood.removeObservers(viewLifecycleOwner)//not expectin real-time updates

                    viewModel.foodCategories.observe(viewLifecycleOwner){
                        rvFoodItemList.startLayoutAnimation()

                        mealAdapter.submitList(it.categories)
                        viewModel.foodCategories.removeObservers(viewLifecycleOwner)
                    }
                }
            }
        }


    }

    /**
     * When a card in the rV is clicked
     */
    override fun onFoodCardClicked(data: FoodCategories) {
        val action = HomeDirections.actionHome2ToMealDetailDialog2(data)
        findNavController().navigate(action)
    }

    /**
     * when a card is checked(checkbox)
     */
    override fun onFoodCardChecked(data: FoodCategories) {
        viewModel.userSelection.selectedFoodCategory = data.strCategory
        mealAdapter.notifyDataSetChanged()
    }

    /**
     * fetching last selected food item (if Any)
     */
    override fun getSelectedFood(): String {
        return viewModel.userSelection.selectedFoodCategory ?: ""
    }
}