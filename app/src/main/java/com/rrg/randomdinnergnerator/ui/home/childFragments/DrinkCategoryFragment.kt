package com.rrg.randomdinnergnerator.ui.home.childFragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.rrg.randomdinnergnerator.R
import com.rrg.randomdinnergnerator.data.drinks.DrinkCategories
import com.rrg.randomdinnergnerator.databinding.DrinkCategoryFragmentBinding
import com.rrg.randomdinnergnerator.ui.home.DrinkAdapter
import com.rrg.randomdinnergnerator.ui.home.HomeViewModel

class DrinkCategoryFragment: Fragment(R.layout.drink_category_fragment), DrinkAdapter.DrinkCardListeners {

    private lateinit var drinkAdapter: DrinkAdapter
    private val viewModel by viewModels<HomeViewModel>(
        {requireParentFragment()}  //sharing viewModel with parent
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = DrinkCategoryFragmentBinding.bind(view)
        drinkAdapter = DrinkAdapter(this)

        binding.apply {
            rvDrinkItemList.layoutManager = StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL)
            rvDrinkItemList.adapter = drinkAdapter
            rvDrinkItemList.setHasFixedSize(true)

            viewModel.fetchSuccessDrink.observe(viewLifecycleOwner){
                if(it){ //data fetched

                    //hide loading progress bar
                    loading.visibility = View.GONE

                    viewModel.fetchSuccessDrink.removeObservers(viewLifecycleOwner) //not expectin real-time updates

                    viewModel.drinkCategories.observe(viewLifecycleOwner){
                        rvDrinkItemList.startLayoutAnimation()
                        drinkAdapter.submitList(it.drinks)
                        viewModel.drinkCategories.removeObservers(viewLifecycleOwner)
                    }
                }
            }
        }

    }


    /**
     * click handler when a drink category is clicked
     */
    override fun onDrinkCardClicked(data: DrinkCategories) {
        viewModel.userSelection.selectedDrinkCategory = data.strCategory
        drinkAdapter.notifyDataSetChanged()
    }

    /**
     * fetching last selected drink item (if Any)
     */
    override fun getSelectedDrink(): String {
        return viewModel.userSelection.selectedDrinkCategory ?: ""
    }

}