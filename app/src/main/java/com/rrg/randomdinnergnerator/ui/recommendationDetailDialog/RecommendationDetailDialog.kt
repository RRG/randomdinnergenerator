package com.rrg.randomdinnergnerator.ui.recommendationDetailDialog

import android.app.Dialog
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.rrg.randomdinnergnerator.R
import com.rrg.randomdinnergnerator.data.enums.DinnerTypes
import com.rrg.randomdinnergnerator.databinding.RecommendationDetailsBinding
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception

@AndroidEntryPoint
class RecommendationDetailDialog : DialogFragment() {

    private val viewModel by viewModels<RecommendationDetailViewModel>()
    private val args by navArgs<RecommendationDetailDialogArgs>()
    private lateinit var selectedCategory: DinnerTypes
    private lateinit var binding: RecommendationDetailsBinding


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val builder = AlertDialog.Builder(requireActivity(), R.style.myFoodDetailDialog)
        binding  = RecommendationDetailsBinding.inflate(LayoutInflater.from(context))
        builder.setView(binding.root)

        val dinner = args.dinner
        if (dinner.drinkID == null && dinner.foodID != null) { //means food item of recommended dinner selected
            selectedCategory = DinnerTypes.FOOD
            viewModel.getFoodDetails(dinner.foodID)
        } else if(dinner.drinkID != null && dinner.foodID == null) { //means drink item of recommended dinner selected
            selectedCategory = DinnerTypes.DRINK
            viewModel.getDrinkDetails(dinner.drinkID)
        }

        viewModel.failedFetch.observe(this){
            if(it){ //API request failed
                Toast.makeText(requireContext(),"error", Toast.LENGTH_SHORT).show()
            }
        }

        when (selectedCategory) {
                DinnerTypes.FOOD -> { loadFoodDetails() }

                DinnerTypes.DRINK -> { loadDrinkDetails() }
            }

        return builder.create()
    }

    /**
     * Load necessary observables for food Item details
     */
    private fun loadFoodDetails() {
        binding.apply {
            viewModel.fetchSuccessRecommendedFood.observe(this@RecommendationDetailDialog) {
                if (it) {//successfully fetched & dialog loaded

                    val foodData = viewModel.recommendedFood.value?.meals?.get(0)
                    if (foodData != null) {

                        Glide.with(requireContext())
                            .load(foodData.strMealThumb)
                            .transition(DrawableTransitionOptions.withCrossFade())
                            .into(ivRecommendationDetailImg)

                        tvRecommendationDetailName.setText(foodData.strMeal)
                        tvRecommendationDetailDescription.gravity = Gravity.START
                        tvRecommendationDetailDescription.setText(foodData.strInstructions)
                        tvRecommendationDetailDescription.movementMethod = ScrollingMovementMethod()

                        if (foodData.strYoutube != null) {//make button red
                            btnVideo.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.red)
                            btnVideo.setOnClickListener {
                                //play youtube vid
                                playYoutubeVideo(foodData.strYoutube)
                            }
                        } else { //disable button
                            btnVideo.setOnClickListener {
                                noVideoAttachedToast()
                            }
                        }

                    }
                }
            }
        }
    }

    /**
     * Load necessary observables for drink Item details
     */
    private fun loadDrinkDetails() {
        binding.apply {
            viewModel.fetchSuccessRecommendedDrink.observe(this@RecommendationDetailDialog) {
                if (it) {//successfully fetched & dialog loaded

                    val drinkData = viewModel.recommendedDrink.value?.drinks?.get(0)
                    if (drinkData != null) {

                        Glide.with(requireContext())
                            .load(drinkData.strDrinkThumb)
                            .transition(DrawableTransitionOptions.withCrossFade())
                            .into(ivRecommendationDetailImg)

                        tvRecommendationDetailName.setText(drinkData.strDrink)
                        tvRecommendationDetailDescription.gravity = Gravity.START
                        tvRecommendationDetailDescription.setText(drinkData.strInstructions)
                        tvRecommendationDetailDescription.movementMethod = ScrollingMovementMethod()

                        if (drinkData.strVideo != null) { //make button red
                            btnVideo.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.red)
                            btnVideo.setOnClickListener {
                                //play youtube vid
                                playYoutubeVideo(drinkData.strVideo)
                            }
                        } else { //disable button
                            btnVideo.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.grey)
                            btnVideo.setOnClickListener {
                                noVideoAttachedToast()
                            }
                        }

                    }
                }
            }
        }
    }

    /**
     * method that launches YouTube intent from video link
     */
    private fun playYoutubeVideo(videoURL:String){
        val webIntent = Intent(Intent.ACTION_VIEW, Uri.parse(videoURL))
        try {
            requireContext().startActivity(webIntent)
        }catch (e:Exception){

        }
    }

    /**
     * method that displays a toast when video links are not availabe
     */
    private fun noVideoAttachedToast(){
        Toast.makeText(context,"No Video Attached !", Toast.LENGTH_SHORT).show()
    }


}