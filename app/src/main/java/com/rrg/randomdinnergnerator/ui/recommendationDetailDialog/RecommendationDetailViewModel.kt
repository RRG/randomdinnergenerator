package com.rrg.randomdinnergnerator.ui.recommendationDetailDialog

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rrg.randomdinnergnerator.data.Repository
import com.rrg.randomdinnergnerator.data.drinks.DrinkDetailsResponse
import com.rrg.randomdinnergnerator.data.food.MealDetailsResponse
import com.rrg.randomdinnergnerator.utils.EspressoIdlingResources
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class RecommendationDetailViewModel @Inject constructor(
    private val repository: Repository
):ViewModel(){


    private val mutableRecommendedFood = MutableLiveData<MealDetailsResponse>() //random food details based on selected category
    val recommendedFood : LiveData<MealDetailsResponse> = mutableRecommendedFood
    val fetchSuccessRecommendedFood = MutableLiveData<Boolean>(false)

    private val mutableRecommendedDrink = MutableLiveData<DrinkDetailsResponse>() //random drink details based on selected category
    val recommendedDrink : LiveData<DrinkDetailsResponse> = mutableRecommendedDrink
    val fetchSuccessRecommendedDrink = MutableLiveData<Boolean>(false)

    val failedFetch = MutableLiveData<Boolean>(false) //any api call failure


    fun getFoodDetails(foodID:String){
        EspressoIdlingResources.increment() /// for testing, allows test case to wait for data
        repository
            .getMealDetails(foodID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                failedFetch.value = true
            }
            .doOnComplete {
                fetchSuccessRecommendedFood.value = true
                EspressoIdlingResources.decrement() /// for testing, allows test case to wait for data
            }
            .subscribe(
                {response->mutableRecommendedFood.value = response},
                {_ -> fetchSuccessRecommendedFood.value = false}
            )
    }

    fun getDrinkDetails(drinkID:String){
        EspressoIdlingResources.increment() /// for testing, allows test case to wait for data
        repository
            .getDrinkDetails(drinkID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                failedFetch.value = true
            }
            .doOnComplete {
                fetchSuccessRecommendedDrink.value = true
                EspressoIdlingResources.decrement() /// for testing, allows test case to wait for data
            }
            .subscribe(
                {response->mutableRecommendedDrink.value = response},
                {_ -> fetchSuccessRecommendedDrink.value = false}
            )
    }

}