package com.rrg.randomdinnergnerator. ui.recommendation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rrg.randomdinnergnerator.data.Repository
import com.rrg.randomdinnergnerator.data.drinks.Drink
import com.rrg.randomdinnergnerator.data.drinks.DrinkDetailsResponse
import com.rrg.randomdinnergnerator.data.drinks.DrinkResponse
import com.rrg.randomdinnergnerator.data.food.Meal
import com.rrg.randomdinnergnerator.data.food.MealDetailsResponse
import com.rrg.randomdinnergnerator.data.food.MealResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject
import com.rrg.randomdinnergnerator.utils.EspressoIdlingResources

@HiltViewModel
class RecommendationViewModel @Inject constructor(
    private val repository: Repository
):ViewModel(){

    //merge all success listeners into a single mediator live data to observe
    val combinedSuccessLists = MediatorLiveData<Boolean>() //STATE for fetching all food & drink lists based on selected category

    private val mutableFoodList = MutableLiveData<MealResponse>()  //food list based on selected category
    val foodList :LiveData<MealResponse> = mutableFoodList
    private val fetchSuccessFood = MutableLiveData<Boolean>(false)

    private val mutableDrinkList = MutableLiveData<DrinkResponse>()  //drink list based on selected category
    val drinkList :LiveData<DrinkResponse> = mutableDrinkList
    private val fetchSuccessDrink = MutableLiveData<Boolean>(false)

    val failedFetch = MutableLiveData<Boolean>(false) //any api call failure

    init { //init all mediator live data
        combinedSuccessLists.addSource(fetchSuccessFood){
            combinedSuccessLists.value = evaluateApiListRequestSuccess()
        }
        combinedSuccessLists.addSource(fetchSuccessDrink){
            combinedSuccessLists.value = evaluateApiListRequestSuccess()
        }
    }

    private fun evaluateApiListRequestSuccess(): Boolean { //simple evaluation to allow waiting for both Food & drink list
        return fetchSuccessFood.value == true && fetchSuccessDrink.value  == true
    }

    /**
     * fetching DRINK items based on category selection made by user
     */
    fun getFoodByCategory(cat:String){
        EspressoIdlingResources.increment() /// for testing, allows test case to wait for data
        repository
            .getMealByCategory(cat)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                failedFetch.value = true
            }
            .doOnComplete {
                fetchSuccessFood.value = true
                EspressoIdlingResources.decrement() /// for testing, allows test case to wait for data
            }
            .subscribe(
                {response->mutableFoodList.value = response},
                {_ -> fetchSuccessFood.value = false} //error
            )
    }

    /**
     * fetching DRINK items based on category selection made by user
     */
    fun getDrinkByCategory(cat:String){
        EspressoIdlingResources.increment() /// for testing, allows test case to wait for data
        repository
            .getDrinkByCategory(cat)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                failedFetch.value = true
            }
            .doOnComplete {
                fetchSuccessDrink.value = true
                EspressoIdlingResources.decrement() /// for testing, allows test case to wait for data
            }
            .subscribe(
                {response->mutableDrinkList.value = response},
                {_ -> fetchSuccessDrink.value = false} //error
            )
    }


    /**
     * method to find a random  Food item within a list
     */
    fun getRandomFood(): Meal? {
        return foodList.value?.meals?.random()
    }

    /**
     * method to find a random Drink item within a list
     */
    fun getRandomDrink(): Drink? {
        return drinkList.value?.drinks?.random()
    }


}