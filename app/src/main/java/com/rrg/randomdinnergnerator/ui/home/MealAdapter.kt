package com.rrg.randomdinnergnerator.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.rrg.randomdinnergnerator.R
import com.rrg.randomdinnergnerator.data.food.FoodCategories
import com.rrg.randomdinnergnerator.databinding.CategoryRecordItemBinding

class MealAdapter(
    private val listener:MealCardListeners
): ListAdapter<FoodCategories, MealAdapter.MealViewHolder>(DiffCallback()) {

    interface MealCardListeners{
        fun onFoodCardClicked(data: FoodCategories)
        fun onFoodCardChecked(data: FoodCategories)
        fun getSelectedFood():String
    }

    inner class MealViewHolder(
        private val binding: CategoryRecordItemBinding
    ):RecyclerView.ViewHolder(binding.root){
        init {
            binding.apply {
                //do stuff here with each card
                root.setOnClickListener {
                    val index = adapterPosition
                    listener.onFoodCardClicked(getItem(index))
                }

                checkBox.setOnClickListener {
                    val record = getItem(adapterPosition)
                    listener.onFoodCardChecked(record)
                }
            }
        }

        fun bindViews(record: FoodCategories){
            binding.apply {
                //assign data to each card
                tvCardItemName.setText(record.strCategory)

                Glide.with(root)
                    .load(record.strCategoryThumb)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .error(R.drawable.ic_baseline_error_24)
                    .into(ivCardThumb)

                //Will retain previous selection in UI, and unselect previous items
                checkBox.isChecked = record.strCategory == listener.getSelectedFood()
            }
        }
    }


    class DiffCallback:DiffUtil.ItemCallback<FoodCategories>(){
        override fun areItemsTheSame(oldItem: FoodCategories, newItem: FoodCategories): Boolean {
            return oldItem.idCategory==oldItem.idCategory
        }

        override fun areContentsTheSame(oldItem: FoodCategories, newItem: FoodCategories): Boolean {
            return oldItem==oldItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):MealViewHolder{
        val binding = CategoryRecordItemBinding.inflate(
            LayoutInflater.from(parent.context),parent,false)
        return MealViewHolder(binding)
    }

    override fun onBindViewHolder(holder:MealViewHolder, position:Int){
        val currentItem = getItem(position)
        holder.bindViews(currentItem)
    }
}