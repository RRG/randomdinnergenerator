package com.rrg.randomdinnergnerator.ui.home

import android.util.Log
import androidx.lifecycle.*
import com.rrg.randomdinnergnerator.data.drinks.DrinkCategoriesResponse
import com.rrg.randomdinnergnerator.data.food.FoodCategoriesResponse
import com.rrg.randomdinnergnerator.data.Repository
import com.rrg.randomdinnergnerator.data.SelectedCategories
import com.rrg.randomdinnergnerator.data.enums.DinnerTypes
import com.rrg.randomdinnergnerator.utils.EspressoIdlingResources
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: Repository
):ViewModel() {

    var currentView = MutableLiveData<DinnerTypes>(DinnerTypes.FOOD) //corresponds to toggle state
    var userSelection = SelectedCategories()

    private val mutableFoodCategories = MutableLiveData<FoodCategoriesResponse>()
    val foodCategories : LiveData<FoodCategoriesResponse> = mutableFoodCategories
    val failedFetch = MutableLiveData<Boolean>(false)
    val fetchSuccessFood = MutableLiveData<Boolean>(false)


    /**
     * fetch food categories from API, and put in a MutableLiveData
     */
    fun getFoodCategories(){
        EspressoIdlingResources.increment() /// for testing, allows test case to wait for data
        repository
            .getMealCategories()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                failedFetch.value = true
            }
            .doOnComplete {
                fetchSuccessFood.value = true
                EspressoIdlingResources.decrement() /// for testing, allows test case to wait for data
            }
            .subscribe(
                {response -> mutableFoodCategories.value = response},  //next
                {err -> onFailure(err)}                                //onError
            )
    }


    private val mutableDrinkCategories = MutableLiveData<DrinkCategoriesResponse>()
    val drinkCategories : LiveData<DrinkCategoriesResponse> = mutableDrinkCategories
    val fetchSuccessDrink = MutableLiveData<Boolean>(false)

    /**
     * fetch drink categories from API, and put in a MutableLiveData
     */
    fun getDrinkCategories(){
        EspressoIdlingResources.increment() /// for testing, allows test case to wait for data
        repository
            .getDrinkCategories()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                failedFetch.value = true
            }
            .doOnComplete {
                fetchSuccessDrink.value = true
                EspressoIdlingResources.decrement() /// for testing, allows test case to wait for data
            }
            .subscribe (
                {response ->mutableDrinkCategories.value  = response },//next
                {error->onFailure(error)}                             //onError
            )
    }


    /**
     * Method that is called when an API calls fails
     */
    private fun onFailure(t: Throwable?) {
        failedFetch.value = true
        Log.i("haha",t.toString())
    }


}