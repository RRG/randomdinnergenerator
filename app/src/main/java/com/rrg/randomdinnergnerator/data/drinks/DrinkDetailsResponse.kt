package com.rrg.randomdinnergnerator.data.drinks

data class DrinkDetailsResponse(
    val drinks:List<DrinkDetails>
)
