package com.rrg.randomdinnergnerator.data.food

data class MealResponse(
    val meals : List<Meal>
)
