package com.rrg.randomdinnergnerator.data.food



data class FoodCategoriesResponse(
    val categories: List<FoodCategories>
)
