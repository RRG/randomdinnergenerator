package com.rrg.randomdinnergnerator.data.food

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Meal(
    val strMeal:String,
    val strMealThumb:String,
    val idMeal: String
):Parcelable
