package com.rrg.randomdinnergnerator.data.drinks

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DrinkCategories(
    val strCategory:String
): Parcelable
