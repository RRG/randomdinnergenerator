package com.rrg.randomdinnergnerator.data

data class ResponseMediator(
    var foodSuccess:Boolean,
    var drinkSuccess:Boolean
)
