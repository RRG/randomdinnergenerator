package com.rrg.randomdinnergnerator.data.food

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MealDetails(
    val idMeal : String,
    val strMeal : String,
    val strCategory : String,
    val strArea : String,
    val strInstructions : String,
    val strMealThumb : String,
    val strYoutube : String?
):Parcelable
