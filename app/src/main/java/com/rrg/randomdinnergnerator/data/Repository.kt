package com.rrg.randomdinnergnerator.data

import com.rrg.randomdinnergnerator.api.DrinkAPI
import com.rrg.randomdinnergnerator.api.MealAPI
import com.rrg.randomdinnergnerator.data.drinks.DrinkCategoriesResponse
import com.rrg.randomdinnergnerator.data.drinks.DrinkDetailsResponse
import com.rrg.randomdinnergnerator.data.drinks.DrinkResponse
import com.rrg.randomdinnergnerator.data.food.FoodCategoriesResponse
import com.rrg.randomdinnergnerator.data.food.MealDetailsResponse
import com.rrg.randomdinnergnerator.data.food.MealResponse
import com.rrg.randomdinnergnerator.dependancyInj.DrinkAPIClient
import com.rrg.randomdinnergnerator.dependancyInj.MealAPIClient
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(
    @MealAPIClient private val mealAPI: MealAPI,
    @DrinkAPIClient private val drinkAPI: DrinkAPI
) {

    fun getMealCategories(): Observable<FoodCategoriesResponse> = mealAPI.getFoodCategories()

    fun getDrinkCategories(): Observable<DrinkCategoriesResponse> = drinkAPI.getDrinkCategories()

    fun getMealByCategory(category:String): Observable<MealResponse> = mealAPI.getFoodListByCategory(category)

    fun getDrinkByCategory(category:String): Observable<DrinkResponse> = drinkAPI.getDrinkByCategory(category)

    fun getMealDetails(mealID:String): Observable<MealDetailsResponse> = mealAPI.getMealDetails(mealID)

    fun getDrinkDetails(drinkID:String): Observable<DrinkDetailsResponse> = drinkAPI.getDrinkDetails(drinkID)
}