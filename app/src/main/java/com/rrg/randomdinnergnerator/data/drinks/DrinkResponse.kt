package com.rrg.randomdinnergnerator.data.drinks

data class DrinkResponse(
    val drinks : List<Drink>
)
