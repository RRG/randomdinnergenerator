package com.rrg.randomdinnergnerator.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RecommendedDinner(
    val foodID:String? = null,
    val drinkID:String? = null
): Parcelable
