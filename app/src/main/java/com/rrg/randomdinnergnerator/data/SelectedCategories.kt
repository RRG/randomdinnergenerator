package com.rrg.randomdinnergnerator.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SelectedCategories(
    var selectedFoodCategory: String? = null,
    var selectedDrinkCategory: String? = null
):Parcelable
