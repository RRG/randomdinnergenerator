package com.rrg.randomdinnergnerator.data.drinks

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Drink(
    val strDrink: String,
    val strDrinkThumb: String,
    val idDrink: String
):Parcelable
