package com.rrg.randomdinnergnerator.data.drinks

data class DrinkCategoriesResponse(
    val drinks: List<DrinkCategories>
)
