package com.rrg.randomdinnergnerator.data.enums

enum class DinnerTypes {
    FOOD, DRINK
}