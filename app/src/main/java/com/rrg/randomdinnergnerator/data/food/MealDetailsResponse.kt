package com.rrg.randomdinnergnerator.data.food

data class MealDetailsResponse(
    val meals:List<MealDetails>
)
