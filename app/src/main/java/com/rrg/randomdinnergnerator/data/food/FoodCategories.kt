package com.rrg.randomdinnergnerator.data.food

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FoodCategories(
   val idCategory: String,
   val strCategory: String,
   val strCategoryThumb: String,
   val strCategoryDescription:String
): Parcelable