package com.rrg.randomdinnergnerator.data.drinks

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DrinkDetails(
    val idDrink : String,
    val strDrink : String,
    val strInstructions : String,
    val strVideo : String?,
    val strAlcohol : String,
    val strDrinkThumb : String
):Parcelable
