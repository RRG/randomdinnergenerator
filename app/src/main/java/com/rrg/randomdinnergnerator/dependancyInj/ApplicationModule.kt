package com.rrg.randomdinnergnerator.dependancyInj

import com.rrg.randomdinnergnerator.api.DrinkAPI
import com.rrg.randomdinnergnerator.api.MealAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier

@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    //fun provideAppScope() = CoroutineScope(SupervisorJob())

    @MealAPIClient
    @Provides
    fun provideMealApiInstance(): MealAPI =
        Retrofit.Builder()
            .baseUrl(MealAPI.BASE_URL)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(MealAPI::class.java)


    @DrinkAPIClient
    @Provides
    fun provideDrinkApiInstance(): DrinkAPI =
        Retrofit.Builder()
            .baseUrl(DrinkAPI.BASE_URL)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(DrinkAPI::class.java)

}

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class MealAPIClient

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class DrinkAPIClient