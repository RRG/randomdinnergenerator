package com.rrg.randomdinnergnerator

import com.rrg.randomdinnergnerator.ui.FoodCategoryRecyclerTest
import com.rrg.randomdinnergnerator.ui.DrinkCategoryRecyclerTest
import com.rrg.randomdinnergnerator.ui.HomeTest
import com.rrg.randomdinnergnerator.ui.RecommendationTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    HomeTest::class,
    FoodCategoryRecyclerTest::class,
    DrinkCategoryRecyclerTest::class,
    RecommendationTest::class
)
class UiTestSuite {

}