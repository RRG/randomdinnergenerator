package com.rrg.randomdinnergnerator.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.rrg.randomdinnergnerator.MainActivity
import com.rrg.randomdinnergnerator.R
import com.rrg.randomdinnergnerator.ui.home.DrinkAdapter
import com.rrg.randomdinnergnerator.ui.home.MealAdapter
import com.rrg.randomdinnergnerator.utils.EspressoIdlingResources
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class HomeTest {

    private val testItem = 1

    @get:Rule
    val activityScenario = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun registerIdleResource(){
        IdlingRegistry.getInstance().register(EspressoIdlingResources.countingIdlingResource)
    }

    @After
    fun unRegisterIdleResource(){
        IdlingRegistry.getInstance().unregister(EspressoIdlingResources.countingIdlingResource)
    }

    @Test
    fun testHomeFragmentsNavigationInit() { //verify if all is init in food section
        //verify that fragment container is loaded
        onView(withId(R.id.fragment_container_home)).check(matches(isDisplayed()))
        //verify that toggle is loaded
        onView(withId(R.id.toggle_parent)).check(matches(isDisplayed()))
    }

    @Test
    fun testHomeToRecommendationFail() { //verify recommendation validation
        //navigate to recommendation fragment
        onView(withId(R.id.btn_suggest_dinner)).perform(click())
        //verify that error message is loaded
        onView(withId(R.id.tv_error)).check(matches(isDisplayed()))
    }

    @Test
    fun testRandomDinnerSelection(){
        //var testDrinkName:String = "Chicken"
        //var testFoodName:String = "Cocktail"

        //click on 2nd item in Food list
        onView(withId(R.id.rv_foodItemList))
            .perform(RecyclerViewActions.actionOnItemAtPosition<MealAdapter.MealViewHolder>
                    (testItem, TestUtils.clickChildViewWithId(R.id.checkBox)))

        //click drink option
        onView(withId(R.id.btn_drinks)).perform(click())

        //click on 2nd item in  Drink list
        onView((withId(R.id.rv_drinkItemList)))
            .perform(RecyclerViewActions.actionOnItemAtPosition<DrinkAdapter.DrinkViewHolder>(
                testItem, click()))

        //click recommend button
        onView(withId(R.id.btn_suggest_dinner)).perform(click())

        //see if parent card is displayed
        onView(withId(R.id.cl_recommendation_card_parent)).check(matches(isDisplayed()))
    }

    @Test
    fun testRecommendationButtonRefresh(){
        testRandomDinnerSelection()

        //get previous recommendation
        val foodRecommendationResult: ViewInteraction = onView(withId(R.id.tv_recommendation_food_name))
        val previouslyRecommendedFood = TestUtils.getText(foodRecommendationResult)

        //click to suggest another recommendation
        onView(withId(R.id.btn_suggest_another_recommendation)).perform(click())

        //see if recommendation change
        onView(withId(R.id.tv_recommendation_food_name)).check(matches(not(withText(previouslyRecommendedFood))))
    }

    @Test
    fun testBackButtonFromDinnerRecommendation(){
        testRandomDinnerSelection()
        pressBack()
        onView(withId(R.id.toggle_parent)).check(matches(isDisplayed()))
    }

    @Test
    fun testIfFoodDrinkSelectionSaved(){
        testRandomDinnerSelection()
        pressBack()

        //verify if choice (Drink) has been saved
        onView(withId(R.id.rv_drinkItemList))
            .check(matches(TestUtils.matchAtPosition(testItem,R.id.cl_small_card_parent, isSelected())))

        //go to food section
        onView(withId(R.id.btn_food)).perform(click())

        //verify if choice (food) has been saved
        onView(withId(R.id.rv_foodItemList))
            .check(matches(TestUtils.matchAtPosition(testItem,R.id.checkBox, isChecked())))

    }

}