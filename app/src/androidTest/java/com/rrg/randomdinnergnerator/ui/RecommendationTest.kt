package com.rrg.randomdinnergnerator.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.rrg.randomdinnergnerator.MainActivity
import com.rrg.randomdinnergnerator.R
import com.rrg.randomdinnergnerator.ui.home.DrinkAdapter
import com.rrg.randomdinnergnerator.ui.home.MealAdapter
import com.rrg.randomdinnergnerator.utils.EspressoIdlingResources
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class RecommendationTest {

    private val testItem = 1

    @get:Rule
    val activityScenario = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun registerIdleResource(){
        IdlingRegistry.getInstance().register(EspressoIdlingResources.countingIdlingResource)
    }

    @After
    fun unRegisterIdleResource(){
        IdlingRegistry.getInstance().unregister(EspressoIdlingResources.countingIdlingResource)
    }

    @Test
    fun testDinnerDetailsShown(){
        randomDinnerSelection()

        //click on food recommendation
        onView(withId(R.id.iv_food_recommendation)).perform(click())

        //verify that food recommendation details are shown
        onView(withId(R.id.tv_recommendation_detail_description)).check(matches(isDisplayed()))

        pressBack() //go back to recommendation

        //click on drink recommendation
        onView(withId(R.id.iv_drink_recommendation)).perform(click())

        //verify that drink recommendation details are shown
        onView(withId(R.id.tv_recommendation_detail_description)).check(matches(isDisplayed()))
    }

    private fun randomDinnerSelection(){
        //click on 2nd item in Food list
        onView(withId(R.id.rv_foodItemList))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<MealAdapter.MealViewHolder>
                (testItem, TestUtils.clickChildViewWithId(R.id.checkBox)))

        //click drink option
        onView(withId(R.id.btn_drinks)).perform(click())

        //click on 2nd item in  Drink list
        onView((withId(R.id.rv_drinkItemList)))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<DrinkAdapter.DrinkViewHolder>(
                testItem, click()))

        //click recommend button
        onView(withId(R.id.btn_suggest_dinner)).perform(click())

        //see if parent card is displayed
        onView(withId(R.id.cl_recommendation_card_parent))
            .check(matches(isDisplayed()))
    }
}