package com.rrg.randomdinnergnerator.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.rrg.randomdinnergnerator.MainActivity
import com.rrg.randomdinnergnerator.R
import com.rrg.randomdinnergnerator.ui.home.MealAdapter
import com.rrg.randomdinnergnerator.utils.EspressoIdlingResources
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class FoodCategoryRecyclerTest {

    private val testItem = 1
    private var testFoodName:String = "Chicken"

    @get:Rule
    val activityScenario = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun registerIdleResource(){
        IdlingRegistry.getInstance().register(EspressoIdlingResources.countingIdlingResource)
    }

    @After
    fun unRegisterIdleResource(){
        IdlingRegistry.getInstance().unregister(EspressoIdlingResources.countingIdlingResource)
    }

    @Test
    fun testIfFoodListIsDisplayed(){
        onView(withId(R.id.rv_foodItemList)).check(matches(isDisplayed()))
    }

    @Test
    fun testItemClickInFoodList(){  //press a specific item in RV..and see if pressing back goes back does nothing (disabled)

        onView((withId(R.id.rv_foodItemList)))
            .perform(actionOnItemAtPosition<MealAdapter.MealViewHolder>(testItem, click()))

        onView(withId(R.id.tv_food_detail_name)).check(matches(withText(testFoodName)))
        pressBack()
        onView(withId(R.id.rv_foodItemList)).check(matches(isDisplayed()))
    }

    @Test
    fun testItemClickInFoodList_alt(){  //press a specific item in RV..and see if pressing "CLOSE button" goes back to RV

        onView((withId(R.id.rv_foodItemList)))
            .perform(actionOnItemAtPosition<MealAdapter.MealViewHolder>(testItem, click()))

        onView(withId(R.id.tv_food_detail_name)).check(matches(withText(testFoodName)))
        onView(withId(R.id.btn_close_food_details)).perform(click())
        onView(withId(R.id.rv_foodItemList)).check(matches(isDisplayed()))
    }

    @Test
    fun testItemCheckInFoodList(){
        //check a food category
        onView(withId(R.id.rv_foodItemList))
            .perform(actionOnItemAtPosition<MealAdapter.MealViewHolder>(testItem, TestUtils.clickChildViewWithId(R.id.checkBox)))

        //verify check state of same item
        onView(withId(R.id.rv_foodItemList))
            .check(matches(TestUtils.matchAtPosition(testItem,R.id.checkBox, isChecked())))
    }


}