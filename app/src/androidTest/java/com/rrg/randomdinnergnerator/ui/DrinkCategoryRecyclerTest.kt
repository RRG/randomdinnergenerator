package com.rrg.randomdinnergnerator.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.rrg.randomdinnergnerator.MainActivity
import com.rrg.randomdinnergnerator.R
import com.rrg.randomdinnergnerator.ui.home.DrinkAdapter
import com.rrg.randomdinnergnerator.utils.EspressoIdlingResources
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class DrinkCategoryRecyclerTest {

    private val testItem = 1
    //private var testDrinkName:String = "Cocktail"

    @get:Rule
    val activityScenario = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun registerIdleResource(){
        IdlingRegistry.getInstance().register(EspressoIdlingResources.countingIdlingResource)
    }

    @After
    fun unRegisterIdleResource(){
        IdlingRegistry.getInstance().unregister(EspressoIdlingResources.countingIdlingResource)
    }

    @Test
    fun testIfDrinkListIsDisplayed(){
        //click drink option
        onView(withId(R.id.btn_drinks)).perform(click())
        //see if RV drink is displayed
        onView(withId(R.id.rv_drinkItemList)).check(matches(isDisplayed()))
    }

    @Test
    fun testItemClickInDrinkList(){
        //click drink option
        onView(withId(R.id.btn_drinks)).perform(click())
        //click on 2nd item in list
        onView((withId(R.id.rv_drinkItemList)))
            .perform(RecyclerViewActions.actionOnItemAtPosition<DrinkAdapter.DrinkViewHolder>(
                    testItem, click()))

        //check if testItem is selected
        onView((withId(R.id.rv_drinkItemList)))
            .check(matches(TestUtils.matchAtPosition(testItem,R.id.cl_small_card_parent, isSelected())))
    }

}